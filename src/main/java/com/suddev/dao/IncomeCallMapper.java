package com.suddev.dao;

import com.suddev.pojo.IncomeCall;
import com.suddev.vo.IncomeCallVo;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface IncomeCallMapper {
    int deleteByPrimaryKey(Integer callId);

    int insert(IncomeCall record);

    int insertSelective(IncomeCall record);

    IncomeCall selectByPrimaryKey(Integer callId);

    int updateByPrimaryKeySelective(IncomeCall record);

    int updateByPrimaryKey(IncomeCall record);

    List<IncomeCallVo> selectSelective(@Param("customerId") Integer customerId, @Param("customerName") String customerName, @Param("callId") Integer callId, @Param("callTime") String callTime);
}