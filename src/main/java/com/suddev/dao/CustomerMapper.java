package com.suddev.dao;

import com.suddev.pojo.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerMapper {
    int deleteByPrimaryKey(Integer customerId);

    int insert(Customer record);

    int insertSelective(Customer record);

    Customer selectByPrimaryKey(Integer customerId);

    int updateByPrimaryKeySelective(Customer record);

    int updateByPrimaryKey(Customer record);

    int updateDeleteInfo(Integer customerId);

    List<Customer> searchInfoSelective(@Param("customerId") Integer customerId,
                                       @Param("realname") String realname,
                                       @Param("sex") Character sex,
                                       @Param("phone") String phone,
                                       @Param("startDate") String startDate,
                                       @Param("incomeCallTime") String incomeCallTime,
                                       @Param("feedBackCallTime") String feedBackCallTime);
}