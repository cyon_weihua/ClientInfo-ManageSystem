package com.suddev.dao;

import com.suddev.pojo.FeedbackCall;
import com.suddev.vo.FeedbackCallVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FeedbackCallMapper {
    int deleteByPrimaryKey(Integer callId);

    int insert(FeedbackCall record);

    int insertSelective(FeedbackCall record);

    FeedbackCall selectByPrimaryKey(Integer callId);

    int updateByPrimaryKeySelective(FeedbackCall record);

    int updateByPrimaryKey(FeedbackCall record);

    List<FeedbackCallVo> selectSelective(@Param("customerId") Integer customerId,
                                         @Param("feebackId") Integer feebackId,
                                         @Param("customerName") Integer customerName,
                                         @Param("callTime") String callTime,
                                         @Param("callFrom") String callFrom);
}