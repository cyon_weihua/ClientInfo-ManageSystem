package com.suddev.pojo;

import java.util.Date;

public class IncomeCall {
    private Integer callId;

    private Integer customerId;

    private String callTime;

    private String callMemo;

    public IncomeCall(Integer callId, Integer customerId, String callTime, String callMemo) {
        this.callId = callId;
        this.customerId = customerId;
        this.callTime = callTime;
        this.callMemo = callMemo;
    }

    public IncomeCall() {
        super();
    }

    public Integer getCallId() {
        return callId;
    }

    public void setCallId(Integer callId) {
        this.callId = callId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    public String getCallMemo() {
        return callMemo;
    }

    public void setCallMemo(String callMemo) {
        this.callMemo = callMemo == null ? null : callMemo.trim();
    }
}