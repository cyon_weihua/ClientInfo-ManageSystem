package com.suddev.pojo;

import java.util.Date;

public class FeedbackCall {
    private Integer callId;

    private Integer customerId;

    private String callTime;

    private String callFrom;

    private String callMemo;

    public FeedbackCall(Integer callId, Integer customerId, String callTime, String callFrom, String callMemo) {
        this.callId = callId;
        this.customerId = customerId;
        this.callTime = callTime;
        this.callFrom = callFrom;
        this.callMemo = callMemo;
    }

    public FeedbackCall() {
        super();
    }

    public Integer getCallId() {
        return callId;
    }

    public void setCallId(Integer callId) {
        this.callId = callId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    public String getCallFrom() {
        return callFrom;
    }

    public void setCallFrom(String callFrom) {
        this.callFrom = callFrom == null ? null : callFrom.trim();
    }

    public String getCallMemo() {
        return callMemo;
    }

    public void setCallMemo(String callMemo) {
        this.callMemo = callMemo == null ? null : callMemo.trim();
    }
}