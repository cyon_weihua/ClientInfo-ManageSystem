package com.suddev.pojo;

import java.util.Date;

public class Customer {
    private Integer customerId;

    private String realname;

    private String sex;

    private String birthday;

    private String phone;

    private String cellphone;

    private String address;

    private String startDate;

    private String memo;

    private String delFlg;

    public Customer(Integer customerId, String realname, String sex, String birthday, String phone, String cellphone, String address, String startDate, String memo, String delFlg) {
        this.customerId = customerId;
        this.realname = realname;
        this.sex = sex;
        this.birthday = birthday;
        this.phone = phone;
        this.cellphone = cellphone;
        this.address = address;
        this.startDate = startDate;
        this.memo = memo;
        this.delFlg = delFlg;
    }

    public Customer() {
        super();
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday == null ? null : birthday.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone == null ? null : cellphone.trim();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg == null ? null : delFlg.trim();
    }
}