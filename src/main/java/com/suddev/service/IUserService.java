package com.suddev.service;

import com.suddev.common.ServerResponse;
import com.suddev.pojo.User;

public interface IUserService {
    ServerResponse<User> login(String username, String password);

    ServerResponse<User> getInformation(String username);
}
