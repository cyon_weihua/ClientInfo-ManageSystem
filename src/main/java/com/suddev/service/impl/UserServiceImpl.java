package com.suddev.service.impl;

import com.suddev.common.ServerResponse;
import com.suddev.dao.UserMapper;
import com.suddev.pojo.User;
import com.suddev.service.IUserService;
import com.suddev.util.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("iUserService")
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ServerResponse<User> login(String username, String password){
        int resultCount = userMapper.checkUsername(username);
        if (resultCount==0){
            return ServerResponse.createByErrorMessage("用户名不存在");
        }
        String MD5Password = MD5Util.MD5EncodeUtf8(password);
        User user = userMapper.selectLogin(username, MD5Password);
        if (user==null){
            return ServerResponse.createByErrorMessage("用户密码错误");
        }
        user.setPassword(StringUtils.EMPTY);//密码置空保护用户隐私
        return ServerResponse.createBySuccess("登录成功",user);
    }

    @Override
    public ServerResponse<User> getInformation(String username){
        User user = userMapper.selectByPrimaryKey(username);
        if (user==null){
            return ServerResponse.createByErrorMessage("找不到当前用户");
        }
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.createBySuccess(user);
    }

}
