package com.suddev.service.impl;

import com.suddev.common.ServerResponse;
import com.suddev.dao.FeedbackCallMapper;
import com.suddev.pojo.FeedbackCall;
import com.suddev.service.IFeedbackCallService;
import com.suddev.vo.FeedbackCallVo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-31 8:33 AM
 **/
@Service("iFeedbackCallService")
public class FeedbackCallServiceImpl implements IFeedbackCallService{

    @Autowired
    private FeedbackCallMapper feedbackCallMapper;

    @Override
    public ServerResponse inputFeedbackCall(FeedbackCall feedbackCall){
        int resultCount = feedbackCallMapper.insert(feedbackCall);
        if (resultCount > 0){
            return ServerResponse.createBySuccessMessage("添加回访记录成功");
        }
        return ServerResponse.createByErrorMessage("添加回访记录出错");
    }

    @Override
    public ServerResponse searchFeedbackCall(Integer customerId, Integer feebackId, Integer customerName, String callTime, String callFrom){
        List<FeedbackCallVo> feedbackCallVoList = feedbackCallMapper.selectSelective(customerId,feebackId,customerName,callTime,callFrom);
        if (CollectionUtils.isEmpty(feedbackCallVoList)){
            return ServerResponse.createByErrorMessage("查询结果为空，请修改查询条件");
        }
        return ServerResponse.createBySuccess(feedbackCallVoList);
    }
}
