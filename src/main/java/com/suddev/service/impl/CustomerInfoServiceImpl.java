package com.suddev.service.impl;

import com.suddev.common.Const;
import com.suddev.common.ServerResponse;
import com.suddev.dao.CustomerMapper;
import com.suddev.pojo.Customer;
import com.suddev.service.ICustomerInfoService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-29 10:17 PM
 **/
@Service("iCustomerInfoService")
public class CustomerInfoServiceImpl implements ICustomerInfoService{

    @Autowired
    private CustomerMapper customerMapper;

    @Override
    public ServerResponse<Customer> inputCustomerInfo(Customer customer){
        customer.setDelFlg("0");
        int resultCount = customerMapper.insert(customer);
        if (resultCount>0){
            return ServerResponse.createBySuccess("添加客户信息成功",customer);
        }
        return ServerResponse.createByErrorMessage("添加客户信息出错");
    }

    @Override
    public ServerResponse<Customer> updateCustomerInfo(Customer customer){
        int resultCount = customerMapper.updateByPrimaryKeySelective(customer);
        if (resultCount>0){
            return ServerResponse.createBySuccess("修改客户信息成功",customer);
        }
        return ServerResponse.createByErrorMessage("修改客户信息出错");
    }

    @Override
    public ServerResponse deleteCustomerInfo(Integer customerId){
        if (customerId==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.ILLEGAL_ARGUMENT.getCode(),Const.ResponseCode.ILLEGAL_ARGUMENT.getDsc());
        }
        int resultCount = customerMapper.updateDeleteInfo(customerId);
        if (resultCount>0){
            return ServerResponse.createBySuccess("删除客户信息成功");
        }
        return ServerResponse.createByErrorMessage("删除客户信息出错");
    }

    @Override
    public ServerResponse<List<Customer>> searchCustomerInfo(Integer customerId,String realname,Character sex,String phone,
                                             String startDate,String incomeCallTime, String feedBackCallTime){
        List<Customer> customerList = customerMapper.searchInfoSelective(customerId, realname, sex, phone, startDate, incomeCallTime, feedBackCallTime);
        if (CollectionUtils.isNotEmpty(customerList)){
            return ServerResponse.createBySuccess(customerList);
        }
        return ServerResponse.createByErrorMessage("查询结果为空，请修改查询条件");
    }

    @Override
    public ServerResponse<Customer> searchCustomerInfoById(Integer customerId){
        if (customerId==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.ILLEGAL_ARGUMENT.getCode(),Const.ResponseCode.ILLEGAL_ARGUMENT.getDsc());
        }
        Customer customer = customerMapper.selectByPrimaryKey(customerId);
        if (customer == null){
            return ServerResponse.createByErrorMessage("没有找到符合条件用户");
        }
        return ServerResponse.createBySuccess(customer);
    }
}
