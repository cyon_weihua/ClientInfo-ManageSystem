package com.suddev.service.impl;
import com.google.common.collect.Lists;
import com.suddev.common.ServerResponse;
import com.suddev.dao.IncomeCallMapper;
import com.suddev.pojo.Customer;
import com.suddev.pojo.IncomeCall;
import com.suddev.vo.IncomeCallVo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.suddev.service.IIncomeCallService;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-31 9:17 AM
 **/
@Service("iIncomeCallService")
public class IncomeCallServiceImpl implements IIncomeCallService {

    @Autowired
    private IncomeCallMapper incomeCallMapper;

    @Override
    public ServerResponse inputIncome(IncomeCall incomeCall){
        int resultCount = incomeCallMapper.insert(incomeCall);
        if (resultCount > 0){
            return ServerResponse.createBySuccessMessage("添加来电记录成功");
        }
        return ServerResponse.createByErrorMessage("添加来电记录出错");
    }

    @Override
    public ServerResponse<List<IncomeCallVo>> searchIncome(Integer customerId, String customerName, Integer callId, String callTime){
        List<IncomeCallVo> incomeCallVoList = incomeCallMapper.selectSelective(customerId, customerName, callId, callTime);
        if (CollectionUtils.isEmpty(incomeCallVoList)){
            return ServerResponse.createByErrorMessage("查询结果为空，请修改查询条件");
        }
        return ServerResponse.createBySuccess(incomeCallVoList);
    }
}
