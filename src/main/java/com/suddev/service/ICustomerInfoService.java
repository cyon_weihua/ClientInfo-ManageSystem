package com.suddev.service;

import com.suddev.common.ServerResponse;
import com.suddev.pojo.Customer;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-29 10:17 PM
 **/
public interface ICustomerInfoService {

    ServerResponse<Customer> inputCustomerInfo(Customer customer);

    ServerResponse<Customer> updateCustomerInfo(Customer customer);

    ServerResponse deleteCustomerInfo(Integer customerId);

    ServerResponse<List<Customer>> searchCustomerInfo(Integer customerId, String realname, Character sex, String phone,
                                                      String startDate, String incomeCallTime, String feedBackCallTime);

    ServerResponse<Customer> searchCustomerInfoById(Integer customerId);
}
