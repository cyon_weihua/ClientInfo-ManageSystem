package com.suddev.service;

import com.suddev.common.ServerResponse;
import com.suddev.pojo.Customer;
import com.suddev.pojo.IncomeCall;
import com.suddev.vo.IncomeCallVo;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-31 9:17 AM
 **/
public interface IIncomeCallService {

    ServerResponse inputIncome(IncomeCall incomeCall);

    ServerResponse<List<IncomeCallVo>> searchIncome(Integer customerId, String customerName, Integer callId, String callTime);
}
