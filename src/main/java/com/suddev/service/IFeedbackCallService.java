package com.suddev.service;

import com.suddev.common.ServerResponse;
import com.suddev.pojo.FeedbackCall;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-31 8:33 AM
 **/
public interface IFeedbackCallService {
    ServerResponse inputFeedbackCall(FeedbackCall feedbackCall);

    ServerResponse searchFeedbackCall(Integer customerId, Integer feebackId, Integer customerName, String callTime, String callFrom);
}
