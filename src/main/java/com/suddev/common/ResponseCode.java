package com.suddev.common;

/**
 * 返回status
 * 枚举类，接口处理状态code和description
 */
public enum ResponseCode {
    SUCCESS(0,"SUCCESS"),
    ERROR(1,"ERROR"),
    NEED_LOGIN(10,"NEED_LOGIN"),
    ILLEGAL_ARGUMENT(2,"ILLEGAL_ARGUMENT");

    public final int code;
    public final String dsc;

    ResponseCode(int code, String dsc) {
        this.code = code;
        this.dsc = dsc;
    }

    public int getCode() {
        return code;
    }

    public String getDsc() {
        return dsc;
    }

}

