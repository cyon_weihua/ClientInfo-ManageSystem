package com.suddev.controller;

import com.suddev.common.Const;
import com.suddev.common.ServerResponse;
import com.suddev.pojo.FeedbackCall;
import com.suddev.pojo.User;
import com.suddev.service.IFeedbackCallService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-31 8:25 AM
 **/
@Controller
@RequestMapping("/feedback/")
public class FeedBackCallController {

    @Autowired
    private IFeedbackCallService iFeedbackCallService;

    @RequestMapping(value = "input_feedback.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse inputFeedbackCall(FeedbackCall feedbackCall, HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iFeedbackCallService.inputFeedbackCall(feedbackCall);
    }

    @RequestMapping(value = "search_feedback.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse searchFeedbackCall(@RequestParam(name = "customerId",required = false) Integer customerId,
                                             @RequestParam(name = "feebackId",required = false)Integer feebackId,
                                             @RequestParam(name = "customerName",required = false)Integer customerName,
                                             @RequestParam(name = "callTime",required = false)String callTime,
                                             @RequestParam(name = "callFrom",required = false)String callFrom,
                                             HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iFeedbackCallService.searchFeedbackCall(customerId, feebackId, customerName, callTime, callFrom);
    }
}
