package com.suddev.controller;

import com.suddev.common.Const;
import com.suddev.common.ServerResponse;
import com.suddev.pojo.IncomeCall;
import com.suddev.pojo.User;
import com.suddev.service.IIncomeCallService;
import com.suddev.vo.IncomeCallVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-31 9:16 AM
 **/
@Controller
@RequestMapping("/income/")
public class IncomeCallController {

    @Autowired
    private IIncomeCallService iIncomeCallService;
    
    @RequestMapping(value = "input_income.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse inputIncome(IncomeCall incomeCall, HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iIncomeCallService.inputIncome(incomeCall);
    }

    @RequestMapping(value = "search_income.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse<List<IncomeCallVo>> inputIncome(Integer customerId, @RequestParam(value = "name") String customerName, Integer callId, String callTime, HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iIncomeCallService.searchIncome(customerId, customerName, callId, callTime);
    }
}
