package com.suddev.controller;

import com.suddev.common.Const;
import com.suddev.common.ServerResponse;
import com.suddev.pojo.Customer;
import com.suddev.pojo.User;
import com.suddev.service.ICustomerInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-07-29 10:13 PM
 **/
@Controller
@RequestMapping("/info/")
public class CustomInfoController {

    @Autowired
    private ICustomerInfoService iCustomerInfoService;

    @RequestMapping(value = "input_info.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse<Customer> inputCustomerInfo(Customer customer, HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iCustomerInfoService.inputCustomerInfo(customer);
    }

    @RequestMapping(value = "update_info.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse<Customer> updateCustomerInfo(Customer customer, HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iCustomerInfoService.updateCustomerInfo(customer);
    }

    @RequestMapping(value = "delete_info.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse deleteCustomerInfo(Integer customerId, HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iCustomerInfoService.deleteCustomerInfo(customerId);
    }

    @RequestMapping(value = "quick_search_info.do",method = {RequestMethod.POST})
    @ResponseBody
    public ServerResponse<List<Customer>> searchCustomerInfo(@RequestParam(name = "customerId",required = false) Integer customerId,
                                                             @RequestParam(name = "name",required = false) String realname,
                                                             @RequestParam(name = "sex",required = false) Character sex,
                                                             @RequestParam(name = "phone",required = false) String phone,
                                                             @RequestParam(name = "startDate",required = false) String startDate,
                                                             @RequestParam(name = "incomeCallTime",required = false) String incomeCallTime,
                                                             @RequestParam(name = "feedBackCallTime",required = false) String feedBackCallTime,
                                                             HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        //如果未传入参数则报参数错误
        if (StringUtils.isNoneBlank(realname,phone, startDate, incomeCallTime, feedBackCallTime) && (sex == null || sex.equals(' ')) && (customerId == null)){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.ILLEGAL_ARGUMENT.getCode(),Const.ResponseCode.ILLEGAL_ARGUMENT.getDsc());
        }
        return iCustomerInfoService.searchCustomerInfo(customerId, realname, sex, phone, startDate, incomeCallTime, feedBackCallTime);
    }

    @RequestMapping(value = "search_info_by_id.do",method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ServerResponse<Customer> searchCustomerInfoById(Integer customerId,HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return iCustomerInfoService.searchCustomerInfoById(customerId);
    }

    @RequestMapping(value = "get_birthday_today_info.do",method = {RequestMethod.POST,RequestMethod.GET})
    @ResponseBody
    public ServerResponse<List<Customer>> getBirthdayTodayInfo(HttpSession session){
        User user = (User) session.getAttribute(Const.CURRENT_USER);
        //禁止未登录用户调用接口
        if (user==null){
            return ServerResponse.createByErrorCodeMessage(Const.ResponseCode.NEED_LOGIN.getCode(),Const.ResponseCode.NEED_LOGIN.getDsc());
        }
        return null;
    }
}
