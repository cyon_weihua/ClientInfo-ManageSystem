package com.suddev.vo;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-08-21 8:37 AM
 **/
public class IncomeCallVo {
    private Integer callId;

    private Integer customerId;

    private String callTime;

    private String callMemo;

    private String customerName;

    public IncomeCallVo() {
    }

    public IncomeCallVo(Integer callId, Integer customerId, String callTime, String callMemo, String customerName) {
        this.callId = callId;
        this.customerId = customerId;
        this.callTime = callTime;
        this.callMemo = callMemo;
        this.customerName = customerName;
    }

    public Integer getCallId() {
        return callId;
    }

    public void setCallId(Integer callId) {
        this.callId = callId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    public String getCallMemo() {
        return callMemo;
    }

    public void setCallMemo(String callMemo) {
        this.callMemo = callMemo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
