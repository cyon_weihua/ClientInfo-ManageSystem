package com.suddev.vo;

/**
 * ${DESCRIPTION}
 *
 * @author suddev
 * @create 2017-08-22 8:20 AM
 **/
public class FeedbackCallVo {
    private Integer callId;

    private Integer customerId;

    private String callTime;

    private String callFrom;

    private String customerName;

    private String callMemo;

    public FeedbackCallVo() {
    }

    public FeedbackCallVo(Integer callId, Integer customerId, String callTime, String callFrom, String customerName, String callMemo) {
        this.callId = callId;
        this.customerId = customerId;
        this.callTime = callTime;
        this.callFrom = callFrom;
        this.customerName = customerName;
        this.callMemo = callMemo;
    }

    public Integer getCallId() {
        return callId;
    }

    public void setCallId(Integer callId) {
        this.callId = callId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    public String getCallFrom() {
        return callFrom;
    }

    public void setCallFrom(String callFrom) {
        this.callFrom = callFrom;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCallMemo() {
        return callMemo;
    }

    public void setCallMemo(String callMemo) {
        this.callMemo = callMemo;
    }
}
