USE managesystem;

CREATE TABLE `tb_user` (
	`username` VARCHAR (20) NOT NULL COMMENT '用户名',
	`password` VARCHAR (20) NOT NULL COMMENT '用户密码，MD5加密',
	`realname` VARCHAR (10) NOT NULL COMMENT '用户真实姓名',
	PRIMARY KEY (`username`)
);

CREATE TABLE `tb_customer` (
	`customer_id` INT (10) NOT NULL AUTO_INCREMENT COMMENT '客户编号',
	`realname` INT (11) NOT NULL COMMENT '客户姓名',
	`sex` CHAR (1) NOT NULL COMMENT '性别',
	`birthday` CHAR (10) NOT NULL COMMENT '客户生日',
	`phone` VARCHAR (15) NOT NULL COMMENT '客户联系电话',
	`cellphone` VARCHAR (15) NOT NULL COMMENT '客户手机号码',
	`address` VARCHAR (100) NOT NULL COMMENT '客户地址',
	`start_date` datetime NOT NULL COMMENT '成为客户日期',
	`memo` VARCHAR (200) NOT NULL COMMENT '注释',
	`del_flg` CHAR (1) DEFAULT 0 COMMENT '删除标志 0-未删除 1-已删除',
	PRIMARY KEY (`customer_id`),
	KEY `customer_id_index` (`customer_id`) USING BTREE
);

CREATE TABLE `tb_income_call` (
	`call_id` INT (10) NOT NULL AUTO_INCREMENT COMMENT '客户来电编号',
	`customer_id` INT (10) NOT NULL COMMENT '客户编号',
	`call_time` datetime NOT NULL COMMENT '客户来电时间',
	`call_memo` VARCHAR (200) NOT NULL COMMENT '来电内容概要',
	PRIMARY KEY (`call_id`),
	KEY `customer_id_index` (`customer_id`) USING BTREE
);

CREATE TABLE `tb_feedback_call` (
	`call_id` INT (10) NOT NULL AUTO_INCREMENT COMMENT '客户来电编号',
	`customer_id` INT (10) NOT NULL COMMENT '客户编号',
	`call_time` datetime NOT NULL COMMENT '客户回访时间',
	`call_from` VARCHAR (20) NOT NULL COMMENT '回访人员',
	`call_memo` VARCHAR (200) NOT NULL COMMENT '来电内容概要',
	PRIMARY KEY (`call_id`),
	KEY `call_id_index` (`call_id`) USING BTREE
);

